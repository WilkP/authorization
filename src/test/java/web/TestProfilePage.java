package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import domain.Priviledge;
import domain.User;

@RunWith(MockitoJUnitRunner.class)
public class TestProfilePage extends Mockito {
	
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private HttpSession session;
	@Mock
	private PrintWriter writer;	
	@Spy	
	@InjectMocks
	private UserProfile servlet;
	
	
	private User user = new User("viapunk", "lol", "email@gmail.com", Priviledge.DEFAULT);
	
	@Test
	public void servlet_should_show_user_data()
		throws IOException, ServletException{
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("conf")).thenReturn(user);
		when(response.getWriter()).thenReturn(writer);
		servlet.doGet(request, response);
		verify(writer).print(
				"Twoja strona profilowa\n"+
						"Nazwa uzytkownika: " + user.getUsername() +
						" Email: " + user.getEmail()+ 
						" Uprawnienia: " + user.getPriviledge().toString());
	}
	
}
