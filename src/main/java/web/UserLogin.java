package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repository.HsqlUserRepository;

import javax.servlet.http.HttpServletRequest;
@WebServlet("/UserLogin")
public class UserLogin extends HttpServlet {
	
	Connection connection;
	HsqlUserRepository allUsers;
	public UserLogin(){
		this.allUsers = new HsqlUserRepository();
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException, NullPointerException{
		
		String login = request.getParameter("username");
		String password = request.getParameter("pass");
		
		User user = allUsers.byUsername(login);
		
		if(user != null){
			if(user.getUsername().equals(login) && user.getPassword().equals(password))
				request.getSession().setAttribute("conf", user);
			else response.getWriter().print("Login fail.");
			//odesłanie do strony profilowej
			response.sendRedirect("/UserProfile");
		}
		else{
			response.getWriter().print("Login fail.");		
		}
		
	}
}
