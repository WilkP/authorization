package repository;

import java.util.List;

import domain.User;

public interface IUserRepository {
	public User byUsername(String username);
	public User byEmail(String email);
	public void add(User user);
	public void updateData(User user);
	public List<User> asAList();
	public void clearTable();
	int count();

}
