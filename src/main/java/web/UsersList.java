package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.*;
import repository.*;

@WebServlet("/UsersList")
public class UsersList extends HttpServlet{
	HsqlUserRepository allUsers;
	
	public UsersList(){
		this.allUsers = new HsqlUserRepository();
	}
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException{
		List<User> users = allUsers.asAList();
		
		response.getWriter().write("<table>"+
				"<tr><td>Username</td> <td>Email</td> <td>Priviledge</td></tr>");
		for(User user : users){
			response.getWriter().write("<tr><td>"+user.getUsername()+"</td>"
					+ "<td>"+user.getEmail()+"</td>"
					+ "<Td>"+user.getPriviledge()+"</td></tr>");
		}
		
		response.getWriter().write("</table>");
		
	}
}
