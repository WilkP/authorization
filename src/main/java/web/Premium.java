package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Priviledge;
import domain.User;
import repository.HsqlUserRepository;

public class Premium extends HttpServlet{
	private HsqlUserRepository allUsers;
	
	public Premium(){
		this.allUsers = new HsqlUserRepository();
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException{
		
		User user = allUsers.byEmail(request.getParameter("username"));
		user.setPriviledge(Priviledge.PREMIUM);
		allUsers.updateData(user);
	}
}
