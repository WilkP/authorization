package web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.EmailValidator;

import domain.Priviledge;
import domain.User;
import repository.HsqlUserRepository;
import repository.IUserRepository;

@WebServlet("/UserRegistration")
public class UserRegistration extends HttpServlet{
	private HsqlUserRepository repository;
	User newUser;
	
	public UserRegistration(){
		this.repository = new HsqlUserRepository();
	}
	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException{
		newUser = retrieveDataFromForm(request);
		
		if(validateFormData(newUser, request)){
			repository.add(newUser);
			request.getSession().setAttribute("conf", newUser);
			response.getWriter().println("Rejestracja zakonczona");
		}else{
			response.getWriter().println("Rejestracja nie powiodla sie, nieprawidlowe dane.");
		}		
	}
	public User retrieveDataFromForm(HttpServletRequest request){
		User result = new User();
		result.setUsername(request.getParameter("username"));
		result.setPassword(request.getParameter("pass"));
		result.setEmail(request.getParameter("email"));
		result.setPriviledge(Priviledge.DEFAULT);
		return result;
	}
	public boolean validateFormData(User data, HttpServletRequest request){
		if (data.getUsername().isEmpty())	return false;
		
		if(data.getEmail().isEmpty())	return false;
		else if(!EmailValidator.getInstance()
				.isValid(data.getEmail()))	return false;
		
		if(data.getPassword().isEmpty()) return false;
		else if(!data.getPassword().equals(request.getParameter("confpass"))) return false;
		
		if(repository.byUsername(data.getUsername()) != null) return false;
		if(repository.byEmail(data.getEmail()) != null) return false;
		return true;
	}
}
