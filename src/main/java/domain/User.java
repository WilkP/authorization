package domain;

public class User {
	
	private String username 		= null;
	private String password 		= null;
	private String email			= null;
	private Priviledge priviledge 	= Priviledge.DEFAULT;
	
	
	
	public User(String username, String password, String email, Priviledge priviledge) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.priviledge = priviledge;
	}
	public User() {
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Priviledge getPriviledge() {
		return priviledge;
	}
	public void setPriviledge(Priviledge priviledge) {
		this.priviledge = priviledge;
	}
	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", email=" + email + ", priviledge="
				+ priviledge + "]";
	}
	@Override
	public boolean equals(Object other) {
	    if (!(other instanceof User)) {
	        return false;
	    }

	    User that = (User) other;

	    // Custom equality check here.
	    return this.username.equals(that.username)
	        && this.password.equals(that.password)
	        && this.email.equals(that.email)
	        && this.priviledge.equals(that.priviledge);
	}
	
	
	

}
