package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import domain.Priviledge;
import domain.User;
import repository.HsqlUserRepository;
import repository.IUserRepository;

@RunWith(MockitoJUnitRunner.class)
public class TestRegistrationServlet extends Mockito{

	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private HttpSession session;
	@Mock
	private PrintWriter writer;
	@Spy
	@InjectMocks
	UserRegistration servlet;
	@Spy
	private HsqlUserRepository repository = new HsqlUserRepository();
	private User user = new User("viapunk", "password", "email@gmail.com", Priviledge.DEFAULT);
	
	
	@Before
	public void setUp() throws IOException{
		when(response.getWriter()).thenReturn(writer);
	}
	@After
	public void tearDown(){
		repository.clearTable();
	}
	
	@Test
	public void servlet_should_verify_retrieved_data_from_form()
		throws IOException, ServletException{
		when(request.getParameter("username")).thenReturn(user.getUsername());
		when(request.getParameter("pass")).thenReturn(user.getPassword());
		when(request.getParameter("email")).thenReturn(user.getEmail());
		when(request.getParameter("confpass")).thenReturn(user.getPassword());
		when(request.getSession()).thenReturn(session);
		servlet.doPost(request, response);
		verify(servlet).validateFormData(user, request);
	}
	@Test
	public void servlet_should_add_data_to_repository()
		throws IOException, ServletException{
		when(request.getParameter("username")).thenReturn(user.getUsername());
		when(request.getParameter("pass")).thenReturn(user.getPassword());
		when(request.getParameter("email")).thenReturn(user.getEmail());
		when(request.getParameter("confpass")).thenReturn(user.getPassword());
		when(request.getSession()).thenReturn(session);
		servlet.doPost(request, response);
		verify(repository).add(user);
	}
	@Test
	public void servlet_should_inform_about_successful_registration()
		throws IOException, ServletException{
		when(request.getParameter("username")).thenReturn(user.getUsername());
		when(request.getParameter("pass")).thenReturn(user.getPassword());
		when(request.getParameter("email")).thenReturn(user.getEmail());
		when(request.getParameter("confpass")).thenReturn(user.getPassword());
		when(request.getSession()).thenReturn(session);
		servlet.doPost(request, response);
		verify(writer).println("Rejestracja zakonczona");
	}
	@Test
	public void servlet_should_inform_about_failed_registration()
		throws IOException, ServletException{
		when(request.getParameter("username")).thenReturn(user.getUsername());
		when(request.getParameter("pass")).thenReturn(user.getPassword());
		when(request.getParameter("email")).thenReturn(user.getEmail());
		when(request.getParameter("confpass")).thenReturn("wrong");
		when(request.getSession()).thenReturn(session);
		servlet.doPost(request, response);
		verify(writer).println("Rejestracja nie powiodla sie, nieprawidlowe dane.");
	}
	@Test
	public void registration_should_fail_if_user_or_email_already_exists()
		throws IOException, ServletException{
		repository.add(user);
		when(request.getParameter("username")).thenReturn(user.getUsername());
		when(request.getParameter("pass")).thenReturn(user.getPassword());
		when(request.getParameter("email")).thenReturn(user.getEmail());
		when(request.getParameter("confpass")).thenReturn(user.getPassword());
		when(request.getSession()).thenReturn(session);
		servlet.doPost(request, response);
		verify(writer).println("Rejestracja nie powiodla sie, nieprawidlowe dane.");
	}
}
