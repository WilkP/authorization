package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import domain.Priviledge;
import domain.User;
import repository.HsqlUserRepository;

@RunWith(MockitoJUnitRunner.class)
public class TestUsersListServlet extends Mockito {

	@Mock
	HttpServletRequest request;
	@Mock
	HttpServletResponse response;
	@Mock
	HttpSession session;
	@Mock
	PrintWriter writer;
	@Mock
	HsqlUserRepository repository;
	@InjectMocks
	UsersList servlet;
	
	List<User> usersList = new ArrayList<User>();
	User user = new User("viapunk", "lol", "pwilk.uk@gmail.com", Priviledge.DEFAULT);
	User user2 = new User("mark", "cukier", "lol@lol.com", Priviledge.ADMINISTRATOR);
	
	@Before
	public void setUp() throws IOException{
		when(request.getSession()).thenReturn(session);
		when(response.getWriter()).thenReturn(writer);
		usersList.add(user);
		usersList.add(user2);
	}
	@After
	public void tearDown(){
		usersList.removeAll(usersList);
	}
	
	@Test
	public void servlet_should_return_users_list()
		throws IOException, ServletException{
		when(repository.asAList()).thenReturn(usersList);
		servlet.doGet(request, response);
		verify(writer).write("<table>"+
				"<tr><td>Username</td> <td>Email</td> <td>Priviledge</td></tr>");
		for(User usery: usersList){
			verify(writer).write("<tr><td>"+usery.getUsername()+"</td>"
					+ "<td>"+usery.getEmail()+"</td>"
					+ "<Td>"+usery.getPriviledge()+"</td></tr>");
		}

	}
	
	
}
