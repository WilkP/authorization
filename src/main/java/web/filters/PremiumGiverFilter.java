package web.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Priviledge;
import domain.User;

/**
 * Servlet Filter implementation class PremiumGiverFilter
 */
@WebFilter("/PremiumGiver.jsp")
public class PremiumGiverFilter implements Filter {

    /**
     * Default constructor. 
     */
    public PremiumGiverFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		User user = (User) httpRequest.getSession().getAttribute("conf");
		if(user !=null){
			if(user.getPriviledge() != Priviledge.ADMINISTRATOR){
				httpResponse.getWriter().println("Dostep zabroniony");
				return;
			}else
				chain.doFilter(request, response);
		}else{
			httpResponse.getWriter().println("Dostep zabroniony");
			return;
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
