package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import domain.Priviledge;
import domain.User;
import repository.HsqlUserRepository;

@RunWith(MockitoJUnitRunner.class)
public class TestLoginServlet extends Mockito{

	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private HttpSession session; 
	@InjectMocks
	UserLogin servlet;
	@Spy	
	private HsqlUserRepository repository = new HsqlUserRepository();
	
	private User user = new User("viapunk", "password", "email@gmail.com", Priviledge.DEFAULT);
	
	@Before	
	public void setUp(){
		repository.add(user);
	}
	@After
	public void tearDown(){
		repository.clearTable();
	}
	
	@Test
	public void servlet_should_set_data_in_session_if_login_and_password_exists_in_db()
		throws IOException, ServletException{
		
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("username")).thenReturn("viapunk");
		when(request.getParameter("pass")).thenReturn("password");
		servlet.doPost(request, response);
		verify(session).setAttribute("conf", user);
	}
	
	@Test
	public void servlet_should_redirect_logged_user_to_profile_page()
		throws IOException, ServletException{
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("username")).thenReturn("viapunk");
		when(request.getParameter("pass")).thenReturn("password");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/UserProfile");
	}
	@Test
	public void servlet_should_inform_about_login_failure()
		throws IOException, ServletException{
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("username")).thenReturn("viapunk");
		when(request.getParameter("pass")).thenReturn("wrongpassword");
		servlet.doPost(request, response);
		verify(writer).print("Login fail.");
	}
	
	
}
