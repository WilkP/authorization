package web.filters;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import domain.Priviledge;
import domain.User;

@RunWith(MockitoJUnitRunner.class)
public class AllFiltersTester extends Mockito{

	@Mock
	HttpServletRequest httpRequest;	
	@Mock
	HttpServletResponse httpResponse;
	@Mock
	HttpSession session;
	@Mock
	User user;
	@Mock
	FilterChain chain;
	
	@Mock
	PrintWriter writer;
	
	@InjectMocks
	LoginFilter loginFilter;
	@InjectMocks
	PremiumFilter premiumFilter;
	@InjectMocks
	PremiumGiverFilter premiumGiverFilter;
	@InjectMocks
	ProfileFilter profileFilter;
	
	
	@Before
	public void setUp() throws IOException{
		when(httpRequest.getSession()).thenReturn(session);
		when(httpRequest.getSession().getAttribute("conf")).thenReturn(user);
		when(httpResponse.getWriter()).thenReturn(writer);
	}
	@Test
	public void login_register_filter_should_redirect_user_to_profile_page()
		throws IOException, ServletException{
		when(user.getPriviledge()).thenReturn(Priviledge.DEFAULT);
		loginFilter.doFilter(httpRequest, httpResponse, chain);
		verify(httpResponse).sendRedirect("/UserProfile");
	}
	@Test
	public void premium_filter_should_redirect_default_user_to_his_profile_page()
		throws IOException, ServletException{
		when(user.getPriviledge()).thenReturn(Priviledge.DEFAULT);
		premiumFilter.doFilter(httpRequest, httpResponse, chain);
		verify(httpResponse).sendRedirect("/UserProfile");
	}
	@Test
	public void premiumgiver_filter_should_allow_only_administrator()
		throws IOException, ServletException{
		when(user.getPriviledge()).thenReturn(Priviledge.ADMINISTRATOR);
		premiumGiverFilter.doFilter(httpRequest, httpResponse, chain);
		verify(chain, times(2)).doFilter(httpRequest, httpResponse);
	}
	@Test
	public void profile_filter_should_block_access_to_strangers() 
			throws IOException, ServletException{
		when(httpRequest.getSession().getAttribute("conf")).thenReturn(null);
		profileFilter.doFilter(httpRequest, httpResponse, chain);
		verify(writer).println("Dostep zabroniony");
	}
}
