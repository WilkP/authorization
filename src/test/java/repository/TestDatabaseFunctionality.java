package repository;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import domain.Priviledge;
import domain.User;

@RunWith(MockitoJUnitRunner.class)
public class TestDatabaseFunctionality extends Mockito{
	
	private User user, user2;
	private HsqlUserRepository repository = new HsqlUserRepository();
	private List<User> usersList = new ArrayList();
	
	@Before
	public void setUp(){
		user = new User("viapunk", "lol", "pwilk.uk@gmail.com", Priviledge.DEFAULT);
		user2 = new User("mark", "cukier", "lol@lol.com", Priviledge.ADMINISTRATOR);
		repository = new HsqlUserRepository();
		usersList.add(user);
		usersList.add(user2);
	}
	@After
	public void tearDown(){
		repository.clearTable();
	}
	
	@Test
	public void repository_constructor_should_initialize_properly(){
		try{
			new HsqlUserRepository();
		}catch(Exception e){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void database_should_save_values(){		
		repository.add(user);
	}
	
	@Test
	public void database_should_load_valid_user_data_by_email(){
		repository.add(user);
		assertEquals(user, repository.byEmail(user.getEmail()));
	}
	@Test
	public void database_should_load_valid_user_data_by_username(){
		repository.add(user);
		assertEquals(user, repository.byUsername(user.getUsername()));
	}
	@Test
	public void database_should_update_data_properly(){
		repository.add(user);
		user.setPriviledge(Priviledge.ADMINISTRATOR);
		repository.updateData(user);
		
		assertEquals(Priviledge.ADMINISTRATOR, repository.byUsername(user.getUsername()).getPriviledge());
	}
	@Test
	public void database_shold_return_list_of_all_users(){
		for(User users: usersList){
			repository.add(users);
		}
		List<User> result = repository.asAList();
		assertThat(usersList, is(result));
	}
	
}
